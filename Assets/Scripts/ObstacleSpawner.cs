﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour {
    public GameObject obstacle;
    public float TimeToSpawn = 2;
    public float maxY = -5;
    public float minY = -8;

    float Timer = 1;
    void Start()
    {

    }


    void Update()
    {
        Timer += Time.deltaTime;

        if (Timer >= TimeToSpawn)
        {
            float randomY = Random.Range(minY, maxY);

            Vector3 spawnPosition = new Vector3(8, randomY, 0);

            Instantiate(obstacle, spawnPosition, Quaternion.identity);

            Timer = 1;

        }

    }
}
    
	
	